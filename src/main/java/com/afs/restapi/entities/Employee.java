package com.afs.restapi.entities;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Double salary;
    private Long companyid;
    private boolean active;

    public Employee() {
    }

    public Employee(Long id, String name, Integer age, String gender, Double salary, boolean active, Long companyid) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyid = companyid;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Long getCompanyid() {
        return companyid;
    }

    public void setCompanyid(Long companyid) {
        this.companyid = companyid;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public boolean AgeisInValid() {
        return this.getAge() < 18 || this.getAge() > 65;
    }

    public boolean ageAndSalaryIsInValid() {
        return this.getAge() > 30 && this.getSalary() < 20000;
    }
}
