package com.afs.restapi.service;

import com.afs.restapi.entities.Employee;
import com.afs.restapi.exception.EmployeeIsInactiveException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.InValidAgeException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Autowired
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee create(Employee employee) {
        if (employee.AgeisInValid()) {
            throw new InValidAgeException();
        }
        if (employee.ageAndSalaryIsInValid()){
            throw new RuntimeException("invalid");
        }
        employee.setActive(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(Long id,Employee employee) throws EmployeeIsInactiveException {
        Employee updateEmployee = findById(id);
        if (updateEmployee == null){
            throw new EmployeeIsInactiveException();
        }
        if (!updateEmployee.isActive()){
            throw new EmployeeIsInactiveException();
        }
        updateEmployee.setSalary(employee.getSalary());
        updateEmployee.setAge(employee.getAge());
        return employeeRepository.update(updateEmployee);

    }


    public void delete(Long id) throws EmployeeNotFoundException {
        Employee employeeToBeRemoved = employeeRepository.findById(id);
        if (employeeToBeRemoved == null) {
            throw new EmployeeNotFoundException();
        }
        employeeToBeRemoved.setActive(false);
        employeeRepository.update(employeeToBeRemoved);
    }
}
