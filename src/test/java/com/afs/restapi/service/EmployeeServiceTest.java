package com.afs.restapi.service;

import com.afs.restapi.entities.Employee;
import com.afs.restapi.exception.EmployeeIsInactiveException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.InValidAgeException;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class EmployeeServiceTest {

    @Test
    void should_not_create_successfully_when_create_employee_given_a_inValid_age() {
        //Given
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "tom", 15, "Female", 1000.0,true,1L);
        //When
        //Then
        Assertions.assertThrows(InValidAgeException.class, () -> {
            employeeService.create(employee);
        });
    }
    @Test
    void should_not_create_successfully_when_create_employee_given_salary_is_below_to_20000_and_age_is_larger_than_30(){
        //Given
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "tom", 33, "Female", 6000D, true, 1L);
        //When
        //Then
        Assertions.assertThrows(RuntimeException.class, () -> {
            employeeService.create(employee);
        });
    }

    @Test
    void should_create_successfully_when_create_employee_given_age_is_22() {
        EmployeeRepository mockedEmployeeRepository = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "tom", 22, "Female", 6000d, true, 1L);
        BDDMockito.given(mockedEmployeeRepository.insert(employee)).willReturn(employee);
        Employee createdEmployee = employeeService.create(employee);
        assertEquals(createdEmployee.getId(), employee.getId());
        assertEquals(createdEmployee.getName(), employee.getName());
    }


    @Test
    void should_return_status_inactive_when_perform_deleteEmployee_given_an_active_employee() throws EmployeeNotFoundException {
        // Given
        EmployeeRepository mockedEmployeeRepository = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "john", 42, "Male", 1000D,true,1L);
        BDDMockito.given(mockedEmployeeRepository.findById(anyLong())).willReturn(employee);
        // When
        employeeService.delete(1L);
        // Then
        verify(mockedEmployeeRepository).update(argThat((employee1) -> {
            assertThat(employee1.getName()).isEqualTo("john");
            assertThat(employee1.getId()).isEqualTo(1);
            assertFalse(employee1.isActive());
            return true;
        }));
    }

    @Test
    void should_throw_exception_when_update_given_active_is_false_employee(){
        EmployeeRepository mockedEmployeeRepository = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(null, "tom", 19, "Female", 6000d, true, 1L);
        employee.setActive(false);
        BDDMockito.given(mockedEmployeeRepository.findById(1L)).willReturn(employee);

        assertThrows(EmployeeIsInactiveException.class, () -> employeeService.update(employee.getId(),employee));
    }

}