package com.afs.restapi.controller;

import com.afs.restapi.entities.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployControllerTest {

    @Resource
    private EmployeeRepository employeeRepository;

    @Resource
    private MockMvc client;

    @BeforeEach
    void clear() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(lzi.getId()))
                .andExpect(jsonPath("$[0].name").value(lzi.getName()))
                .andExpect(jsonPath("$[0].age").value(lzi.getAge()))
                .andExpect(jsonPath("$[0].gender").value(lzi.getGender()))
                .andExpect(jsonPath("$[0].salary").value(lzi.getSalary()))
                .andExpect(jsonPath("$[0].companyid").value(lzi.getCompanyid()))
        ;
    }

    @Test
    void should_return_employees_with_gender_when_perform_findEmployeeByGender_given_two_employee_with_different_gender() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        Employee lucy = new Employee(null, "lucy", 21, "Female", 5000.0,true,1L);
        employeeRepository.insert(lucy);
        employeeRepository.insert(lzi);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Male"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(lzi.getId()))
                .andExpect(jsonPath("$[0].name").value(lzi.getName()))
                .andExpect(jsonPath("$[0].age").value(lzi.getAge()))
                .andExpect(jsonPath("$[0].gender").value(lzi.getGender()))
                .andExpect(jsonPath("$[0].salary").value(lzi.getSalary()))
                .andExpect(jsonPath("$[0].companyid").value(lzi.getCompanyid()))
        ;
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_json() throws Exception {
        // Given
        String json = "{\n" +
                "    \"name\": \"xioaming\",\n" +
                "    \"age\": 27,\n" +
                "    \"gender\": \"Female\",\n" +
                "    \"salary\": 10,\n" +
                "    \"active\": true,\n" +
                "    \"companyId\": 1\n" +
                "}";
        // When
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                // Then
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("xioaming"))
                .andExpect(jsonPath("$.age").value(27))
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").value(10));
    }


    @Test
    void should_return_employee_when_perform_findById_given_id() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        Employee lucy = new Employee(null, "lucy", 21, "Female", 5000.0,true,1L);
        employeeRepository.insert(lucy);
        employeeRepository.insert(lzi);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", lucy.getId()))
                // Then
                .andExpect(jsonPath("$.id").value(lucy.getId()))
                .andExpect(jsonPath("$.name").value(lucy.getName()))
                .andExpect(jsonPath("$.age").value(lucy.getAge()))
                .andExpect(jsonPath("$.gender").value(lucy.getGender()))
                .andExpect(jsonPath("$.salary").value(lucy.getSalary()));
    }

    @Test
    void should_return_updated_employee_when_perform_update_given_employee_and_id() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        Employee updateEmployee = new Employee();
        updateEmployee.setSalary(1000D);
        String updateEmployeeJson = new ObjectMapper().writeValueAsString(updateEmployee);
        // When
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", lzi.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateEmployeeJson))
                // Then
                .andExpect(jsonPath("$.id").value(lzi.getId()))
                .andExpect(jsonPath("$.name").value(lzi.getName()))
                .andExpect(jsonPath("$.age").value(lzi.getAge()))
                .andExpect(jsonPath("$.gender").value(lzi.getGender()))
                .andExpect(jsonPath("$.salary").value(1000));
    }

    @Test
    void should_return_status_204_when_perform_delete_given_id() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        // When
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", lzi.getId()))
                // Then
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_employees_when_findByPage_given_page_size() throws Exception {
        // Given
        Employee lzi = new Employee(null, "lzi", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        Employee lili = new Employee(null, "lili", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        Employee lucy = new Employee(null, "lucy", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        Employee tom = new Employee(null, "tom", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        Employee john = new Employee(null, "john", 21, "Male", 5000.0,true,1L);
        employeeRepository.insert(lzi);
        // When
        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("page", "1").param("size", "2"))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(lzi.getId()))
                .andExpect(jsonPath("$[0].name").value(lzi.getName()))
                .andExpect(jsonPath("$[0].age").value(lzi.getAge()))
                .andExpect(jsonPath("$[0].gender").value(lzi.getGender()))
                .andExpect(jsonPath("$[0].salary").value(lzi.getSalary()));
    }

}
